#Analysing the Phonotactic Learner's blick output for all the Data from Exp 3 simultaneously 
library(dplyr)
library(lme4)
library(ggplot2)

#*****************
#Initial data manipulation

#Setting Working Directory
setwd("/Users/Karthik/Documents/Research/Adam-FeatureLearning/BitBucket/UCLAPhonotacticLearner/FeatureLearning_Exp3/")

#Opening and extracting the relevant data
fileNames=list.files("Results/",pattern=".txt")
fileNames = fileNames[grepl("blick",fileNames)]
Data=NULL
#i=fileNames[1]
for(i in fileNames){
  data_temp=read.csv(paste("Results/",i,sep=""),sep="\t",header=F) %>%
            filter(!(row_number() %in% c(1:3))) %>%
            mutate(Consonants = V1,
                   Score = V2,
                   Type = V14,
                   Sub = i) %>%
            select(Sub, Consonants, Score, Type)
  Data=rbind(Data,data_temp)
}
head(Data)

#*****************
#Data Manipulation
Data2 = Data %>%
        mutate(Score=as.numeric(as.character(Score))) %>%
        mutate(MaxentValue=exp(-Score))  #Implementing H&W, Score -> Maxent value

#Changing level order and names
Data2$TYPE = factor(Data2$Type, levels=c("NoPatternControls", "VoicingControls", "StopControls", "NewConsTestStimuli", "NewWordTestStimuli", "OldTestStimuli"))
levels(Data2$TYPE) = c("Disharmony", "OnlyVoicing", "OnlyCont", "NewCStims", "NewWStims", "OldStims")


Data_PL_SubjectAvg = Data2 %>%
                  mutate(participant = gsub("blickTestResults_Sub","",Sub),
                         participant = gsub(".txt","",participant),
                         participant = as.integer(participant)) %>%
                  group_by(participant,TYPE) %>%  #Getting averages
                  summarise(MeanMaxentValue = mean(MaxentValue))
unique(Data_PL_SubjectAvg$participant)
nrow(Data_PL_SubjectAvg)

#Reading actual subject responses
Data_Actual_SubjectAvg = read.csv("Analysis/Exp3Results.csv")
head(Data_Actual_SubjectAvg)
nrow(Data_Actual_SubjectAvg)

#Merging UCLA PL output with actual subject output
Data_All = inner_join(Data_PL_SubjectAvg,Data_Actual_SubjectAvg)
head(Data_All)
nrow(Data_All)

#Changing level order and names
Data_All$TYPE = factor(Data_All$TYPE, levels=c("Disharmony", "OnlyVoicing", "OnlyCont", "NewCStims", "NewWStims", "OldStims"))

#The Maxent values are not directly comparable. They need to be scaled. Read Hayes& Wilson (2008), pg. 399 for more info. 
#Getting the best fit scaling factor for Maxent scores
scalingFactorRange=seq(0.1,10.0,by=0.1)
corrs=NULL
for(i in scalingFactorRange){
  corrs_temp=cor(Data_All$MeanYes,(Data_All$MeanMaxentValue)^(1/i))
  corrs = rbind(corrs,data.frame(scalingFactor=i,corr = corrs_temp))
}

#Plotting Scaling factors and correlations
plot(corrs)
finalScalingFactor = corrs$scalingFactor[corrs$corr == max(corrs$corr)]

Data_All = Data_All %>%
            mutate(scaledMaxentValue = (MeanMaxentValue)^(1/finalScalingFactor))
head(Data_All)

#Overall averages and SE's
Data_Overall = Data_All %>%
                group_by(TYPE) %>%
                summarise(SE = sd(scaledMaxentValue)/sqrt(length(scaledMaxentValue)),
                          MeanScaledMaxentValue = mean(scaledMaxentValue))

(plot=ggplot(Data_Overall, aes(TYPE, MeanScaledMaxentValue)) + geom_bar(stat="identity", fill = "#B22613") + 
  geom_errorbar(aes(ymin=MeanScaledMaxentValue-SE, ymax=MeanScaledMaxentValue+SE, width=0.1)) +
  ylab("Mean Scaled Maxent Values") +
  ggtitle(paste("Scaled Maxent values for different type (scaling factor = ",finalScalingFactor,")",sep="")))

ggsave("Analysis/VSH_Exp3_PhonotacticLearnerPredictions.png", plot, width=6, height=4, dpi=200)

#****************
#Is there an interaction for voicing and stopping?
coding=data.frame(TYPE=c("NewStims","Disharmony","OldStims","OnlyCont","OnlyVoicing"),
                  Voicing=c(1,0,1,0,1),
                  Stopping=c(1,0,1,1,0))
coding$TYPE=c("NewCStims","Disharmony","OldStims","OnlyCont","OnlyVoicing")
coding$Voicing=factor(coding$Voicing)
coding$Stopping=factor(coding$Stopping)

#Merging data with coding
Data_interaction=inner_join(Data_All,coding) %>%
  filter(TYPE!="OldStims")
Data_interaction$TYPE=factor(Data_interaction$TYPE,level=c("Disharmony","NewCStims","OnlyCont","OnlyVoicing"))

head(Data_interaction)
Data_interaction %>%
  group_by(TYPE) %>%
  summarise(MeanYes = mean(scaledMaxentValue))

#Simple anova style analysis - scaled maxent values
m1 = glm(scaledMaxentValue~Voicing*Stopping,data=Data_interaction,family="binomial")
summary(m1)

#****************
#Hold-out set Scaling factor
scalingFactorRange=seq(0.1,10.0,by=0.1)
Data_All2 = Data_All
Data_All2$RowNum = 1:nrow(Data_All)
part1=sample(1:nrow(Data_All),nrow(Data_All)*0.6)
Data_model_tuning = Data_All2 %>% filter(RowNum %in% part1)
Data_model_testing = Data_All2 %>% filter(!(RowNum %in% part1))

corrs=NULL
for(i in scalingFactorRange){
  corrs_temp=cor(Data_model_tuning$MeanYes,(Data_model_tuning$MeanMaxentValue)^(1/i))
  corrs = rbind(corrs,data.frame(scalingFactor=i,corr = corrs_temp))
}

#Plotting Scaling factors and correlations
plot(corrs)
(finalScalingFactor = corrs$scalingFactor[corrs$corr == max(corrs$corr)])

Data_model_testing = Data_model_testing %>%
  mutate(scaledMaxentValue = (MeanMaxentValue)^(1/finalScalingFactor))
head(Data_model_testing)

#Overall averages and SE's
Data_Overall2 = Data_model_testing %>%
  group_by(TYPE) %>%
  summarise(SE = sd(scaledMaxentValue)/sqrt(length(scaledMaxentValue)),
            MeanScaledMaxentValue = mean(scaledMaxentValue))

(plot=ggplot(Data_Overall2, aes(TYPE, MeanScaledMaxentValue)) + geom_bar(stat="identity", fill = "#B22613") + 
  geom_errorbar(aes(ymin=MeanScaledMaxentValue-SE, ymax=MeanScaledMaxentValue+SE, width=0.1)) +
  ylab("Mean Scaled Maxent Values") +
  ggtitle(paste("Scaled Maxent values for different type (scaling factor = ",finalScalingFactor,")",sep="")))