word	score	*[-sonorant,+continuant,+voice,+distributed]	*[+nasal,+dorsal]	*[-strident][-approximant]	*[-anterior][-approximant]	*[-strident][-round,-distributed,-lateral]	*[+voice][-approximant]	*[-anterior][-round,-distributed,-lateral]	*[-word_boundary][+delayed_release]	*[-spread_gl,-anterior,-dorsal][-distributed,-lateral]	*[-word_boundary][-sonorant,+voice]	*[+continuant,+voice][-word_boundary]	*[-labial,-strident,-dorsal][+consonantal]	*[+distributed][+consonantal]	*[+sonorant][-word_boundary]	*[+continuant,-labiodental,-distributed][+continuant,-round,-lateral]	*[+delayed_release,+voice][-word_boundary]	*[+continuant,+voice,+strident]	*[+voice][-distributed,-lateral]	*[+continuant,-spread_gl,-strident][-distributed,-lateral]	*[-continuant,+delayed_release][-word_boundary]	annotation
word	score	default	default	default	default	default	default	default	default	default	default	default	default	default	default	default	default	default	default	default	default	annotation
word	score	4.049	5.594	1.718	1.705	4.579	0.445	1.454	4.538	4.927	5.332	2.824	5.128	4.177	5.322	5.078	2.639	2.688	2.854	3.569	4.229	annotation
G R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
K R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
S T 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
S M 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
P R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
S L 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
F L 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
B L 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
D R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
T R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
F R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
S P 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
S N 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
S P 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
SH R 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
G L 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
M R 	5.322	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	
SH L 	4.177	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	
S K 	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	
V R 	5.463	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	
S R 	5.078	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	
V L 	5.463	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	
M L 	5.322	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	
SH T 	12.262	0	0	0	1	0	0	1	0	1	0	0	0	1	0	0	0	0	0	0	0	
F P 	17.952	0	0	1	1	1	0	1	0	1	0	0	0	0	0	0	0	0	0	1	0	
ZH R 	12.2	1	0	0	0	0	0	0	0	0	0	1	0	0	0	0	1	1	0	0	0	
F SH 	7.961	0	0	1	1	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	
SH N 	12.262	0	0	0	1	0	0	1	0	1	0	0	0	1	0	0	0	0	0	0	0	
F T 	17.952	0	0	1	1	1	0	1	0	1	0	0	0	0	0	0	0	0	0	1	0	
Z R 	13.229	0	0	0	0	0	0	0	0	0	0	1	0	0	0	1	1	1	0	0	0	
N R 	5.322	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	
SH M 	12.262	0	0	0	1	0	0	1	0	1	0	0	0	1	0	0	0	0	0	0	0	
S F 	9.616	0	0	0	0	0	0	0	1	0	0	0	0	0	0	1	0	0	0	0	0	
Z L 	8.151	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	1	1	0	0	0	
Z T 	11.45	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	1	1	1	0	0	
F S 	22.49	0	0	1	1	1	0	1	1	1	0	0	0	0	0	0	0	0	0	1	0	
V Z 	39.272	0	0	1	1	1	1	1	1	1	1	1	0	0	0	0	1	1	1	1	0	
V Z 	39.272	0	0	1	1	1	1	1	1	1	1	1	0	0	0	0	1	1	1	1	0	
ZH L 	16.376	1	0	0	0	0	0	0	0	0	0	1	0	1	0	0	1	1	0	0	0	
SH F 	16.8	0	0	0	1	0	0	1	1	1	0	0	0	1	0	0	0	0	0	0	0	
Z N 	11.45	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	1	1	1	0	0	
F N 	17.952	0	0	1	1	1	0	1	0	1	0	0	0	0	0	0	0	0	0	1	0	
F K 	17.952	0	0	1	1	1	0	1	0	1	0	0	0	0	0	0	0	0	0	1	0	
V T 	26.714	0	0	1	1	1	1	1	0	1	0	1	0	0	0	0	1	0	1	1	0	
Z V 	26.398	0	0	0	0	0	1	0	1	0	1	1	0	0	0	1	1	1	1	0	0	
Z M 	11.45	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	1	1	1	0	0	
ZH M 	27.761	1	0	0	1	0	1	1	0	1	0	1	0	1	0	0	1	1	1	0	0	
F M 	17.952	0	0	1	1	1	0	1	0	1	0	0	0	0	0	0	0	0	0	1	0	
SH P 	12.262	0	0	0	1	0	0	1	0	1	0	0	0	1	0	0	0	0	0	0	0	
V M 	26.714	0	0	1	1	1	1	1	0	1	0	1	0	0	0	0	1	0	1	1	0	
V N 	26.714	0	0	1	1	1	1	1	0	1	0	1	0	0	0	0	1	0	1	1	0	
SH K 	12.262	0	0	0	1	0	0	1	0	1	0	0	0	1	0	0	0	0	0	0	0	
Z P 	11.45	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	1	1	1	0	0	
ZH P 	27.761	1	0	0	1	0	1	1	0	1	0	1	0	1	0	0	1	1	1	0	0	
ZH T 	27.761	1	0	0	1	0	1	1	0	1	0	1	0	1	0	0	1	1	1	0	0	
ZH K 	27.761	1	0	0	1	0	1	1	0	1	0	1	0	1	0	0	1	1	1	0	0	
ZH N 	27.761	1	0	0	1	0	1	1	0	1	0	1	0	1	0	0	1	1	1	0	0	
Z K 	11.45	0	0	0	0	0	1	0	0	0	0	1	0	0	0	0	1	1	1	0	0	
V P 	26.714	0	0	1	1	1	1	1	0	1	0	1	0	0	0	0	1	0	1	1	0	
V K 	26.714	0	0	1	1	1	1	1	0	1	0	1	0	0	0	0	1	0	1	1	0	
ZH V 	37.631	1	0	0	1	0	1	1	1	1	1	1	0	1	0	0	1	1	1	0	0	
