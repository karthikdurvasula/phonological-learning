; This file is a record of the settings for a run of the phonotactic learning program, indicating time of run and the settings that were used.
; Where a field is left blank, it means that the default setting for the program was used (see manual).
; 
; Location of the input files:  /Users/Karthik/Documents/Research/PhonologicalLearning/PhonotacticLearnerUCLA/EnglishOnsets
; Time and date that the program ran:  Wed Aug 19 12:09:53 EDT 2020

Task 0
FeatureFile EnglishFeatures.txt
LearningFile EnglishLearningDataExpanded.txt
TestFile ScholesTestingData_modified.txt
ProjectionsFile 
NGramConstraints EnglishTrigramLimitation.txt
UserConstraints 
MaxConstraints 20
Sigma2 
LearningSampleSize 
MaxGramSize 3
MaxOE 0.35
Smoothing 
MaxPath 
AllowComplements false
