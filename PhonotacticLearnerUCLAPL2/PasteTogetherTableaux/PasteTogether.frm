VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Paste Together Tableaux"
   ClientHeight    =   7635
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8820
   Icon            =   "PasteTogether.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7635
   ScaleWidth      =   8820
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtExcelLocation 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1320
      TabIndex        =   6
      Text            =   "C:\Program Files\Microsoft Office\OFFICE11\excel.exe"
      Top             =   6240
      Width           =   6495
   End
   Begin VB.TextBox txtColinSoftwareLocation 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1320
      TabIndex        =   5
      Text            =   "C:\Colin2"
      Top             =   4800
      Width           =   6375
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1320
      TabIndex        =   4
      Text            =   "Testing.txt"
      Top             =   3360
      Width           =   6375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Go"
      Height          =   855
      Left            =   1320
      TabIndex        =   0
      Top             =   720
      Width           =   6375
   End
   Begin VB.Label Label4 
      Caption         =   "In this box you may past the name of program that will auto-open the output of this program:"
      Height          =   375
      Left            =   1320
      TabIndex        =   7
      Top             =   5520
      Width           =   6375
   End
   Begin VB.Label Label3 
      Caption         =   $"PasteTogether.frx":08CA
      Height          =   735
      Left            =   1320
      TabIndex        =   3
      Top             =   4080
      Width           =   6495
   End
   Begin VB.Label Label2 
      Caption         =   $"PasteTogether.frx":095A
      Height          =   615
      Left            =   1320
      TabIndex        =   2
      Top             =   2640
      Width           =   6375
   End
   Begin VB.Label Label1 
      Caption         =   "Paste together the outputs of Colin Wilson's current phonotactic learner into one coherent file."
      Height          =   615
      Left            =   1320
      TabIndex        =   1
      Top             =   2040
      Width           =   6255
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

    Static ButtonStatus As Boolean
    
    Dim OutputFileName As String
    
    Dim MyLine As String
    Dim NumberOfTableauLines
    Dim Tableau() As String
    Dim TableauForms() As String
    
    Dim NumberOfTestForms As Long
    Dim TestForms() As String
    Dim Annotations() As String
    Dim MyTestForm As String
    
    Dim StarStrucWeight As Single
    Dim ColinScore As Single, RevisedScore As Single
    
    Dim MyTableauLine As String
    Dim LinesProcessed As Long
    Dim MaximumNumberOfTabs As Long
    Dim TestIndex As Long, TableauIndex As Long, TabIndex As Long
    
    'Use same button to exit.
        If ButtonStatus = True Then End
        Let ButtonStatus = True
    
    'Open the Colin-generated file that you need.
        Open txtColinSoftwareLocation.Text + "/tableau.txt" For Input As #1
        
    'Record what file you used to interpret.
        Open App.Path + "/LastFileOpened.txt" For Output As #2
        Print #2, Text1.Text
        Close #2
    
    'Read the lines from Colin's output tableau.
        Do While Not EOF(1)
            Line Input #1, MyLine
            Let NumberOfTableauLines = NumberOfTableauLines + 1
            ReDim Preserve Tableau(NumberOfTableauLines)
            Let Tableau(NumberOfTableauLines) = MyLine
            ReDim Preserve TableauForms(NumberOfTableauLines)
            Let TableauForms(NumberOfTableauLines) = s.Chomp(MyLine)
        Loop
        Close #1
    
    'Open the output file for this program.  Use the user's file name if requested, else default.
        If Text1.Text = "Testing.txt" Then
            Let OutputFileName = "InterpretedTableau.txt"
        Else
            Let OutputFileName = "InterpretedTableauFor" + Text1.Text + ".txt"
        End If
        Open App.Path + "/" + OutputFileName For Output As #2
        
    'Examine the grammar.txt file, created by Colin's software, and print its content (constraints, weights, violations) to the
    '   output file of this program, reformatted as a header.
        'Constraints:
            Print #2, "Const.:"; Chr(9); Chr(9); Chr(9); Chr(9);
            Open txtColinSoftwareLocation.Text + "/grammar.txt" For Input As #1
            Line Input #1, MyLine
            Do While Not EOF(1)
                Line Input #1, MyLine
                Let MyLine = s.Residue(MyLine)
                Print #2, Chr(9); s.Chomp(MyLine);
            Loop
            Print #2,
            Close #1
        'Weights.  Here, we need to grab the weight for *Struc, since it seems to throw off the predictions.
            Let StarStrucWeight = -1000000
            Print #2, "Weights:"; Chr(9); Chr(9); Chr(9); Chr(9);
            Open txtColinSoftwareLocation.Text + "/grammar.txt" For Input As #1
            Line Input #1, MyLine
            Do While Not EOF(1)
                Line Input #1, MyLine
                Let MyLine = s.Residue(s.Residue(MyLine))
                'Grab the first one; afterwards criterion will never be met since weights cannot be negative.
                    If StarStrucWeight = -1000000 Then
                        Let StarStrucWeight = Val(s.Chomp(MyLine))
                    End If
                Print #2, Chr(9); s.Chomp(MyLine);
            Loop
            Print #2,
            Close #1
        'Violations in training data:
            Print #2, "TrainViols:"; Chr(9); Chr(9); Chr(9); "Hx"; Chr(9); "No *Str";
            Open txtColinSoftwareLocation.Text + "/grammar.txt" For Input As #1
            Line Input #1, MyLine
            Do While Not EOF(1)
                Line Input #1, MyLine
                Let MyLine = s.Residue(s.Residue(s.Residue(MyLine)))
                Print #2, Chr(9); s.Chomp(MyLine);
            Loop
            Print #2,
            Close #1
    
    
    'Open the testing file.
        Open App.Path + "/" + Text1.Text For Input As #1
    
    'Look up the outcome in the testing file in Colin's output file, and amalgamate.
        Do While Not EOF(1)
            Line Input #1, MyLine
            Let NumberOfTestForms = NumberOfTestForms + 1
            ReDim Preserve TestForms(NumberOfTestForms)
            ReDim Preserve Annotations(NumberOfTestForms)
            Let TestForms(NumberOfTestForms) = s.Chomp(MyLine)
            Let Annotations(NumberOfTestForms) = s.Residue(MyLine)
            If NumberOfTabs(Annotations(NumberOfTestForms)) > MaximumNumberOfTabs Then
                Let MaximumNumberOfTabs = NumberOfTabs(Annotations(NumberOfTestForms))
            End If
        Loop
    
    'Add tabs to the end of the annotations so they line up nicely.
        For TestIndex = 1 To NumberOfTestForms
            For TabIndex = NumberOfTabs(Annotations(TestIndex)) + 1 To MaximumNumberOfTabs
                Let Annotations(TestIndex) = Annotations(TestIndex) + Chr(9)
            Next TabIndex
        Next TestIndex
        
    'Create the main content of the output file, with the tableaux lines appended to the original test file, aligned nicely by tabs.
        For TestIndex = 1 To NumberOfTestForms
            
            'Load the word for which you are searching for a match in a local variable.
                Let MyTestForm = TestForms(TestIndex)
                
            'Report progress.
                Let LinesProcessed = LinesProcessed + 1
                If LinesProcessed / 100 = Int(LinesProcessed / 100) Then
                    Let Command1.Caption = "Done with " + Str(LinesProcessed) + " lines."
                End If
            
            'Go through all the lines in Colin's tableau file, looking for a match.
                For TableauIndex = 1 To NumberOfTableauLines
                    If TableauForms(TableauIndex) = MyTestForm Then
                        'You've found the form in Colin's output file, so print out the relevant information.
                            Let MyTableauLine = Tableau(TableauIndex)
                            'The test form itself:
                                Print #2, MyTestForm;
                            'The classificatory annotations:
                                Print #2, Chr(9); Annotations(TestIndex);
                            'Repeat the blick form from Colin's file for safety:
                                Print #2, Chr(9); s.Chomp(MyTableauLine);
                                Let MyTableauLine = s.Residue(MyTableauLine)
                            'Print the score that Colin's program computes:
                                Let ColinScore = Val(s.Chomp(MyTableauLine))
                                Print #2, Chr(9); s.Chomp(MyTableauLine);
                                Let MyTableauLine = s.Residue(MyTableauLine)
                            'Compute and print a score that leaves out the effects of *Struc:
                                Let RevisedScore = ColinScore + StarStrucWeight * Val(s.Chomp(MyTableauLine))
                                If Abs(RevisedScore) < 0.005 Then Let RevisedScore = 0
                                Print #2, Chr(9); RevisedScore;
                            'Print out the rest of line unaltered.
                                Print #2, Chr(9); MyTableauLine
                            GoTo ExitPoint
                    End If      'Is this form in Colin's output file the one you were looking for?
                Next TableauIndex
            
            'If you got this far, report trouble, giving user option of leaving.
                Select Case MsgBox("Caution:  no form in tableau.txt found to match " + MyTestForm + ".", vbOKCancel)
                    Case vbCancel
                        Close
                        End
                End Select
ExitPoint:
        Next TestIndex
        Close
        
        'Shell txtExcelLocation + " " + App.Path + "/" + OutputFileName
        
        Let Command1.Caption = "Done.  The output file is in this folder, entitled " + OutputFileName + ".  Click this button again to exit."
    
End Sub


Function NumberOfTabs(MyString As String) As Long

    Dim i As Long
    Dim Dummy As Long
    
    For i = 1 To Len(MyString)
        If Mid(MyString, i, 1) = Chr(9) Then
            Let Dummy = Dummy + 1
        End If
    Next i
    Let NumberOfTabs = Dummy
    
End Function

Private Sub Form_Load()
    'Find the name of the last file worked on.
    On Error GoTo ErrorPoint
    Dim MyLine As String
    Open App.Path + "/LastFileOpened.txt" For Input As #1
    Line Input #1, MyLine
    Let Text1.Text = MyLine
    Close #1
    Exit Sub
ErrorPoint:
    Exit Sub
End Sub
