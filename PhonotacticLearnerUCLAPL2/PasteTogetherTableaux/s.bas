Attribute VB_Name = "s"
Public Function TabTrim(MyLine As String) As String
    Dim Buffer As String
    Let Buffer = MyLine
Restart1:
    If Right(Buffer, 1) = 1 Then
        Let Buffer = Left(Buffer, Len(Buffer) - 1)
        GoTo Restart1
    End If
Restart2:
    If Left(Buffer, 1) = 1 Then
        Let Buffer = Mid(Buffer, 2)
        GoTo Restart2
    End If
    Let TabTrim = Buffer
End Function

Public Function Chomp(MyString) As String
    Dim i As Long
    For i = 1 To Len(MyString)
        If Mid(MyString, i, 1) = Chr(9) Then
            Let Chomp = Left(MyString, i - 1)
            Exit Function
        End If
    Next i
    Let Chomp = MyString
End Function
Public Function Residue(MyString) As String
    Dim i As Long
    For i = 1 To Len(MyString)
        If Mid(MyString, i, 1) = Chr(9) Then
            Let Residue = Mid(MyString, i + 1)
            Exit Function
        End If
    Next i
    Let Residue = ""
End Function

Public Function SlashFinalPath(MyString) As String
    'Make sure a path ends in \.
        Let MyString = Trim(MyString)
        Select Case Right(MyString, 1)
            Case "\", "/"
                Let SlashFinalPath = MyString
            Case Else
                Let SlashFinalPath = MyString + "\"
        End Select
End Function

Public Function NoSlashFinalPath(MyString) As String
    'Make sure a path does *not* end in \ or /.
        Let MyString = Trim(MyString)
        Select Case Right(MyString, 1)
            Case "\", "/"
                Let NoSlashFinalPath = Left(MyString, Len(MyString) - 1)
            Case Else
                Let NoSlashFinalPath = MyString
        End Select
End Function

Public Sub CheckThatFileIsPresent(MyFile As String, Description As String)

    'Warn, and abort, if a crucial file is absent.
        If Dir(Trim(MyFile)) = "" Then
            MsgBox "Problem:  you've specified " + Description + " in the location " + _
            Chr(10) + Chr(10) + _
            mLocationOfInputFiles + MyFile + _
            Chr(10) + Chr(10) + _
            "but I can't find this file.  Kindly correct this problem and then start again.  When you click OK, the program will exit."
            End
        End If

End Sub

Public Function IsAnInteger(MyString As String) As Boolean
    Dim Buffer As String, i As Long
    Let Buffer = Trim(MyString)
    For i = 1 To Len(Buffer)
        Select Case Mid(Buffer, i, 1)
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                'do nothing
            Case Else
                Let IsAnInteger = False
                Exit Function
        End Select
    Next i
    Let IsAnInteger = True
    
End Function

Public Function CustomChomp(MyString As String, Delimitor As String) As String
    'Return the portion of MyString that precedes Delimitor.
    Dim i As Long
    For i = 1 To Len(MyString) - Len(Delimitor) + 1
        If Mid(MyString, i, Len(Delimitor)) = Delimitor Then
            Let CustomChomp = Left(MyString, i - 1)
            Exit Function
        End If
    Next i
    Let CustomChomp = MyString
End Function
Public Function CustomResidue(MyString As String, Delimitor As String) As String
    'Return the portion of MyString that follows Delimitor.
    Dim i As Long
    For i = 1 To Len(MyString) - Len(Delimitor) + 1
        If Mid(MyString, i, Len(Delimitor)) = Delimitor Then
            Let CustomResidue = Mid(MyString, i + Len(Delimitor))
            Exit Function
        End If
    Next i
    Let CustomResidue = ""
End Function

Public Function TrimToPath(MyPath)
    'Chop off an actual file name to get path.
    Dim i As Long
    For i = Len(MyPath) To 1 Step -1
        Select Case Mid(MyPath, i, 1)
            Case "/", "\"
                Let TrimToPath = Left(MyPath, i - 1)
                Exit Function
        End Select
    Next i
    Let TrimToPath = MyPath
End Function
Public Function TrimToFileName(MyPath)
    'Chop off a path to get the plain file name.
    Dim i As Long
    For i = Len(MyPath) To 1 Step -1
        Select Case Mid(MyPath, i, 1)
            Case "/", "\"
                Let TrimToFileName = Mid(MyPath, i + 1)
                Exit Function
        End Select
    Next i
    Let TrimToFileName = MyPath
End Function

Public Function IsPositiveReal(MyString As String) As Boolean

    'Check that a string is a positive real number.
    
    Dim i As Long, NumberOfDecimalPoints As Long
    
    For i = 1 To Len(MyString)
        Select Case Mid(MyString, i, 1)
            Case "."
                Let NumberOfDecimalPoints = NumberOfDecimalPoints + 1
            Case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
                'Do nothing.
            Case Else
                Let IsPositiveReal = False
                Exit Function
        End Select
    Next i
    If NumberOfDecimalPoints <= 1 Then Let IsPositiveReal = True

End Function

Public Sub UnloadAllForms(Optional FormToIgnore As String = "")
  
  'From http://techrepublic.com.com/5100-3513_11-5533338.html#.
    'Try to unload all the forms so that you don't get crap in memory after
    '   execution--said crap is making it hard to copy and delete versions
    '   of the code.

  Dim f As Form
  For Each f In Forms
    If f.Name <> FormToIgnore Then
      Unload f
      Set f = Nothing
    End If
  Next f
    
End Sub
 

Public Function FileDate(FileSpec As String)
    
    On Error GoTo CheckError
    
    'From the VB6 help file.  Other stuff is there for perhaps future use.
        Dim fs, f, s
        Set fs = CreateObject("Scripting.FileSystemObject")
        Set f = fs.GetFile(FileSpec)
        s = UCase(FileSpec) & vbCrLf
        s = s & "Created: " & f.DateCreated & vbCrLf
        s = s & "Last Accessed: " & f.DateLastAccessed & vbCrLf
        s = s & "Last Modified: " & f.DateLastModified
        Let FileDate = f.DateLastModified
        Exit Function
        
CheckError:
    Let FileDate = "unknown"
    Exit Function
    
End Function

Function NumberContainedIn(MyString As String, MyCharacter As String) As Long

    'How many spaces, hyphens, etc. does a string contain?
    
    Dim i
    
    For i = 1 To Len(MyString)
        If Mid(MyString, i, 1) = MyCharacter Then
            Let NumberContainedIn = NumberContainedIn + 1
        End If
    Next i

End Function
