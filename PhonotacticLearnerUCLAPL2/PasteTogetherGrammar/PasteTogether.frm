VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Paste Together"
   ClientHeight    =   8805
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8820
   Icon            =   "PasteTogether.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8805
   ScaleWidth      =   8820
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox textExcelLocation 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1320
      TabIndex        =   7
      Text            =   "C:\Program Files\Microsoft Office\OFFICE11\excel.exe"
      Top             =   7440
      Width           =   6375
   End
   Begin VB.TextBox txtColinProgramFolder 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1320
      TabIndex        =   6
      Text            =   "C:\Colin2"
      Top             =   3120
      Width           =   6375
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1320
      TabIndex        =   2
      Text            =   "AnnotatedTempGrammar.txt"
      Top             =   4920
      Width           =   6375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Go"
      Height          =   855
      Left            =   1320
      TabIndex        =   0
      Top             =   720
      Width           =   6375
   End
   Begin VB.Label Label5 
      Caption         =   "The output will auto-open if you list the program you want to use (e.g. Excel) here:"
      Height          =   375
      Left            =   1320
      TabIndex        =   8
      Top             =   6840
      Width           =   6615
   End
   Begin VB.Label Label4 
      Caption         =   "List here the folder where Colin Wilson's current (2010) phonotactic learner is running:"
      Height          =   495
      Left            =   1320
      TabIndex        =   5
      Top             =   2640
      Width           =   6735
   End
   Begin VB.Label Label3 
      Caption         =   "Program output will appear in a file in the program folder, same name as above but prefixed with ""Interpreted""."
      Height          =   615
      Left            =   1320
      TabIndex        =   4
      Top             =   6000
      Width           =   6495
   End
   Begin VB.Label Label2 
      Caption         =   $"PasteTogether.frx":08CA
      Height          =   615
      Left            =   1320
      TabIndex        =   3
      Top             =   3960
      Width           =   6375
   End
   Begin VB.Label Label1 
      Caption         =   $"PasteTogether.frx":0995
      Height          =   615
      Left            =   1320
      TabIndex        =   1
      Top             =   2040
      Width           =   6255
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

    On Error GoTo ErrorPoint
    Dim MyLine As String
    'Read last file opened.
        Open App.Path + "/LastFileOpened.txt" For Input As #1
        Line Input #1, MyLine
        Let Text1.Text = MyLine
        Close #1
        Exit Sub
        
ErrorPoint:
    Exit Sub

End Sub


Private Sub Command1_Click()

    Static ButtonStatus As Boolean
    
    Dim OutputFileName As String
    
    Dim MyLine As String
    Dim NumberOfColinGrammarLines
    Dim ColinConstraints() As String
    Dim ColinWeights() As Single
    Dim ColinViolationCounts() As String
    Dim ColinConstraintsCheckIfIncluded() As Boolean
    
    'Variables for stastical testing of constraints:
        Dim StatisticallyTestedConstraint() As String
        Dim ColinPlog() As String
        Dim ColinChiSquare() As String
        Dim ColinPValue() As String
        Dim ColinSignificance() As String
        Dim PlogAll As String
        Dim NumberOfStatisticalTests As Long
    
    Dim MyConstraint As String
    
    Dim MaximumNumberOfTabs As Long
    Dim TabIndex As Long, ConstraintIndex As Long, LineIndex As Long
    
    'Use same button to exit.
        If ButtonStatus = True Then End
        Let ButtonStatus = True
        
    'Print that you are working.
        Let Command1.Caption = "Working..."
    
    'Save the file name you're working with.
        Open App.Path + "/LastFileOpened.txt" For Output As #2
        Print #2, Text1.Text
        Close #2
    
    'Open the grammar file, just created by Colin's program.
        Open txtColinProgramFolder.Text + "/grammar.txt" For Input As #1
    
    'Read the lines from Colin's output grammar.
        'Ignore Colin's header line
            Line Input #1, MyLine
        Do While Not EOF(1)
            Line Input #1, MyLine
            'Ignore first column:
                Let MyLine = s.Residue(MyLine)
            'Colin constraints:
                Let NumberOfColinGrammarLines = NumberOfColinGrammarLines + 1
                ReDim Preserve ColinConstraints(NumberOfColinGrammarLines)
                Let ColinConstraints(NumberOfColinGrammarLines) = Trim(s.Chomp(MyLine))
            'Colin weights:
                Let MyLine = s.Residue(MyLine)
                ReDim Preserve ColinWeights(NumberOfColinGrammarLines)
                Let ColinWeights(NumberOfColinGrammarLines) = Val(s.Chomp(MyLine))
            'Colin violation counts:
                Let MyLine = s.Residue(MyLine)
                ReDim Preserve ColinViolationCounts(NumberOfColinGrammarLines)
                Let ColinViolationCounts(NumberOfColinGrammarLines) = s.Chomp(MyLine)
            'Checkoff that the Colin constraints were all found:
                ReDim ColinConstraintsCheckIfIncluded(NumberOfColinGrammarLines)
        Loop
        Close #1
        
    'Now open up the program trace and obtain the statistical significance material.
        Open txtColinProgramFolder.Text + "/programtrace.txt" For Input As #1
        Do While Not EOF(1)
            Line Input #1, MyLine
            'The plog of the full constraint set.
                If Left(MyLine, 3) = "all" Then
                    Let PlogAll = s.Chomp(s.Residue(MyLine))
                ElseIf Left(MyLine, 2) = "- " Then
                    'Otherwise, a constraint is evaluated as a line beginning hyphen + space.
                        Let NumberOfStatisticalTests = NumberOfStatisticalTests + 1
                        'Redim the arrays that store this information.
                            ReDim Preserve StatisticallyTestedConstraint(NumberOfStatisticalTests)
                            ReDim Preserve ColinPlog(NumberOfStatisticalTests)
                            ReDim Preserve ColinChiSquare(NumberOfStatisticalTests)
                            ReDim Preserve ColinPValue(NumberOfStatisticalTests)
                            ReDim Preserve ColinSignificance(NumberOfStatisticalTests)
                        'Parse the line and store the values.
                            Let MyLine = Mid(MyLine, 3)
                            Let StatisticallyTestedConstraint(NumberOfStatisticalTests) = s.Chomp(MyLine)
                            Let MyLine = s.Residue(MyLine)
                            Let ColinPlog(NumberOfStatisticalTests) = s.Chomp(MyLine)
                            Let MyLine = s.Residue(MyLine)
                            Let ColinChiSquare(NumberOfStatisticalTests) = s.Chomp(MyLine)
                            Let MyLine = s.Residue(MyLine)
                            Let ColinPValue(NumberOfStatisticalTests) = s.Chomp(MyLine)
                            Let MyLine = s.Residue(MyLine)
                            Let ColinSignificance(NumberOfStatisticalTests) = s.Chomp(MyLine)
                End If
        Loop
        Close #1
    
    
    'Open the output file for this program.  Use the user's file name if requested, else default.
        If Text1.Text = "Testing.txt" Then
            Let OutputFileName = "InterpretedGrammar.txt"
        Else
            Let OutputFileName = "Interpreted" + Text1.Text
        End If
        Open App.Path + "/" + OutputFileName For Output As #2
    
    'Open the constraint annotation file and find the maximum number of columns
        
        Open App.Path + "/" + Text1.Text For Input As #1
        Do While Not EOF(1)
            Line Input #1, MyLine
            If NumberOfTabs(MyLine) > MaximumNumberOfTabs Then Let MaximumNumberOfTabs = NumberOfTabs(MyLine)
        Loop
        Close #1
        
    'Reopen and grab the header
        Open App.Path + "/" + Text1.Text For Input As #1
        Line Input #1, MyLine
    
    'Print a header for the output file.
        Print #2, "Index";
        Print #2, Chr(9); MyLine;
        For TabIndex = NumberOfTabs(MyLine) To MaximumNumberOfTabs - 1
            Print #2, Chr(9);
        Next TabIndex
        Print #2, Chr(9); "Wght.";
        Print #2, Chr(9); "Viols.";
        Print #2, Chr(9); "Disc. order";
        Print #2, Chr(9); "Plog";
        Print #2, Chr(9); "ChiSquare";
        Print #2, Chr(9); "p";
        Print #2, Chr(9); "Significance"
        
    'Open it up again and look up the counterpart in the Colin grammar to the constraints it includes.
    '   If you find such a constraint, appends its weights and violation counts.
        Do While Not EOF(1)
            Line Input #1, MyLine
                Let MyLine = Trim(MyLine)
                'Print a line number.
                    Let LineIndex = LineIndex + 1
                    Print #2, Trim(Str(LineIndex));
                'Recapitulate the commentary.
                    Print #2, Chr(9); MyLine;
                'Hunt for this in the Colin grammar.
                    Let MyConstraint = Trim(s.Chomp(MyLine))
                    For ConstraintIndex = 1 To NumberOfColinGrammarLines
                        If MyConstraint = ColinConstraints(ConstraintIndex) Then
                            'Check off that you did find it.
                                Let ColinConstraintsCheckIfIncluded(ConstraintIndex) = True
                            'Add enough tabs to achieve horizontal alignment
                                For TabIndex = NumberOfTabs(MyLine) To MaximumNumberOfTabs
                                    Print #2, Chr(9);
                                Next TabIndex
                            'Print the Colinian material.
                                'This one is just for debugging:
                                '    Print #2, ColinConstraints(ConstraintIndex);
                                'Weights and counts:
                                    Print #2, ColinWeights(ConstraintIndex);
                                    Print #2, Chr(9); ColinViolationCounts(ConstraintIndex);
                                'Discovery order:
                                    Print #2, Chr(9); ConstraintIndex;
                            Exit For
                        End If
                    Next ConstraintIndex
                'Hunt for this in the significance testing.
                    For ConstraintIndex = 1 To NumberOfStatisticalTests
                        If MyConstraint = StatisticallyTestedConstraint(ConstraintIndex) Then
                            'print the significance tests
                                'This one is just for debugging
                                '    Print #2, Chr(9); StatisticallyTestedConstraint(ConstraintIndex);
                                'Plog, etc.
                                    Print #2, Chr(9); ColinPlog(ConstraintIndex);
                                    Print #2, Chr(9); ColinChiSquare(ConstraintIndex);
                                    Print #2, Chr(9); ColinPValue(ConstraintIndex);
                                    Print #2, Chr(9); ColinSignificance(ConstraintIndex);
                        End If
                    Next ConstraintIndex
                'Finish the entry:
                    Print #2,
        Loop
        Close #1
        
        
        'Append a copy of the parameters.params file.
            Open txtColinProgramFolder.Text + "/parameters.params" For Input As #1
                Do While Not EOF(1)
                    'Grab the text.
                        Line Input #1, MyLine
                    'Continue the line numbering of the output file, for sortability.
                        Let LineIndex = LineIndex + 1
                        Print #2, Trim(Str(LineIndex));
                    'Identify this for what it is.
                        Print #2, Chr(9); "zzzControlFile";
                    'Add the content.  You need a space because Excel gets confused by the initial hyphens.
                        Print #2, Chr(9); " "; MyLine
                Loop
                Close #1
        
        'Print the plog for the grammar as a whole.
            Print #2, Trim(Str(LineIndex + 1));
            Print #2, Chr(9); "zzzzPlog of entire grammar";
            Print #2, Chr(9); PlogAll
                        
            
            Close #2
        
        'Report to user any constraints not found in the annotation file.
            For ConstraintIndex = 1 To NumberOfColinGrammarLines
                If ColinConstraintsCheckIfIncluded(ConstraintIndex) = False Then
                    If MsgBox("Caution:  the constraint " + ColinConstraints(ConstraintIndex) + " was present in the output file, but not in the annotation file " + Text1.Text + ".  Continue?", _
                        vbYesNo) = vbNo Then
                        Close
                        End
                    End If
                End If
            Next ConstraintIndex
            
        'Call OpenOutputFile(OutputFileName)
        
        Let Command1.Caption = "Done.  The output file is in this folder, entitled " + OutputFileName + ".  Click this button again to exit."
    
End Sub


Function NumberOfTabs(MyString As String) As Long

    Dim i As Long
    Dim Dummy As Long
    
    For i = 1 To Len(MyString)
        If Mid(MyString, i, 1) = Chr(9) Then
            Let Dummy = Dummy + 1
        End If
    Next i
    Let NumberOfTabs = Dummy
    
End Function

Sub OpenOutputFile(MyOutputFileName As String)

On Error GoTo ErrorPoint

    Shell textExcelLocation.Text + " " + App.Path + "/" + MyOutputFileName
    Exit Sub
    
ErrorPoint:
    MsgBox "Trouble opening output file with your program; please open " + MyOutputFileName + " (in program folder) by hand."
    End
    
End Sub

