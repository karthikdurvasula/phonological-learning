Index	Constraint	Constraint Family	Description	Source	Wght.	Viols.	Disc. order	Plog	ChiSquare	p	Significance
1	*[-Strong,+Accent]	Primitive, urmetrics	Don't fill a W position with stress.	Kiparsky and Hanson 1996	 2.937 	1,233	 1 
2	*[+Strong,-Accent]		Don't fill an S position with stressless.	Kiparsky and Hanson 1996
3	*[+Strong,+Rise]		Don't rise in stress out of an S position.	Jespersen 1933
4	*[-Strong,+Fall]		Don't fall in stress out of a W position.	Jespersen 1900
5	*[+Strong,-Fall][]		Don't fail to fall in stress out of an S position.	Implicit in Jespersen 1933	 0.278 	2,605	 23 
6	*[-Strong,-Rise][]		Don't fail to rise out of a W position.	Implicit in Jespersen 1900	 0 	2,865	 5 
7	*[-Accent][-Strong,+Accent]		Simple stressless-stress mismatch	Implicit in Jespersen 1933
8	*[-Strong,+Accent][-Accent]		Simple stress-stressless mismatch	Implicit in Jespersen 1933
9	*[+Strong,+Rise][+Fall]	Stress maximum constraints	Simple stress maximum 	Implicit in Halle and Keyser 1966
10	*[+Strong,+Rise,-J5][+Fall,-J5]		Simple stress maximum, bounded by I-phrase	Halle and Keyser 1966
11	*[+Strong,+Rise,-J4][+Fall,-J4]		Simple stress maximum, bounded by P-phrase	Halle and Keyser 1966
12	*[+Strong,+Rise,-J3][+Fall,-J3]		Simple stress maximum, bounded by Clitic Group	Implicit in Halle and Keyser 1966
13	*[+Strong,+Rise,-J2][+Fall,-J2]		Simple stress maximum, bounded by Word	Implicit in Halle and Keyser 1966
14	*[+Strong,-Accent][+Accent][-Accent]		Strict stress maximum	implicit in Halle and Keyser (1971, 169)
15	*[+Strong,-Accent,-J5][+Accent,-J5][-Accent]		Strict stress maximum, bounded by I-phrase	Halle and Keyser (1971, 169)
16	*[+Strong,-Accent,-J4][+Accent,-J4][-Accent]		Strict stress maximum, bounded by P-phrase	Halle and Keyser (1971, 169)
17	*[+Strong,-Accent,-J3][+Accent,-J3][-Accent]		Strict stress maximum, bounded by Clitic Group	implicit in Halle and Keyser (1971, 169)
18	*[-Accent][-Strong,+Accent,-J2]		Lexical stress maximum I	Fabb and Halle 2008
19	*[+Strong,+Rise,-J2][][-Accent]		Lexical stress maximum II	Fabb and Halle 2008
20	*[-Accent,-J5][-Strong,+Accent,-J2]		Lexical stress maximum I, bounded by I-phrase	implicit in Fabb and Halle 2008
21	*[+Strong,+Rise,-J2][-J5][-Accent]		Lexical stress maximum II, bounded by I-phrase	implicit in Fabb and Halle 2008
22	*[-Accent,-J4][-Strong,+Accent,-J2]		Lexical stress maximum I, bounded by P-phrase	implicit in Fabb and Halle 2008
23	*[+Strong,+Rise,-J2][-J4][-Accent]		Lexical stress maximum II, bounded by P-phrase	implicit in Fabb and Halle 2008
24	*[-Accent,-J3][-Strong,+Accent,-J2]		Lexical stress maximum I, bounded by Clitic Group	implicit in Fabb and Halle 2008
25	*[+Strong,+Rise,-J2][-J3][-Accent]		Lexical stress maximum II, bounded by Clitic Group	implicit in Fabb and Halle 2008
26	*[+Strong,+Rise,-J2]	Word-bounded stress matching	Don't rise out of S, within a simplex word	Kiparsky 1975	 2.587 	3	 19 
27	*[-Strong,+Accent,-J2]		Don't' fall out of W, within a simplex word.	implicit in Kiparsky 1975
28	*[-J5][-Strong,+Accent,-J2]	Inversion constraints	Same, if no [IP break precedes 	Hayes 1983
29	*[-J4][-Strong,+Accent,-J2]		Same, if no [P break precedes 	Hayes 1983
30	*[-J3][-Strong,+Accent,-J2]		Same, if no [CG break precedes 	Hayes 1983	 1.441 	1	 27 
31	*[-J5][-Strong,+Fall,-J3]		same as above but CG level of mismatch	
32	*[-J4][-Strong,+Fall,-J3]		same as above but CG level of mismatch	
33	*[-J5][-Strong,+Fall]		Don't fall out of S unless I break precedes	BH, added for symmetry	 0.837 	191	 12 
34	*[-J4][-Strong,+Fall]		Don't fall out of S unless P break precedes	BH, added for symmetry
35	*[-J3][-Strong,+Fall]		Don't fall out of S unless C break precedes	BH, added for symmetry
36	*[+Realsyl][-Strong,+Accent,-J2]		Don't fall out of W in simplex word unless line-initial	Tarlinskaja
37	*[+Realsyl][-Strong,+Fall]		Don't fall out of W unless line-initial	Tarlinskaja
38	*[+Strong,-Accent,+Rise,-J5][+J5]	Phrase-final constraints	Don't mismatch stressless-stress at end of I-phrase	Kiparsky 1977	 1.937 	1	 24 
39	*[+Strong,-Accent,+Rise,-J4][+J4]		Don't mismatch stressless-stress at end of P-phrase	Kiparsky 1977
40	*[+Strong,-Accent,+Rise,-J3][+J3]		Don't mismatch stressless-stress at end of C-group	Kiparsky 1977
41	*[+Strong,+Rise,-J5][+J5]		Don't mismatch rising stress at end of I-phrase	Kiparsky 1977
42	*[+Strong,+Rise,-J4][+J4]		Don't mismatch rising stress at end of P-phrase	Kiparsky 1977
43	*[+Strong,+Rise,-J3][+J3]		Don't mismatch rising stress at end of C-group	Kiparsky 1977
44	*[+Strong,+Rise,-J2][+J5]		Don't mismatch rising lexical stress at end of I-phrase	Hayes 1989 - for Shelley
45	*[+Strong,+Rise,-J2][+J4]		Don't mismatch rising lexical stress at end of P-phrase	Hayes 1989 - for Shelley
46	*[+Strong,+Rise,-J2][+J3]		Don't mismatch rising lexical stress at end of CG	Hayes 1989 - for Shelley
47	*[+Strong,+Accent,-J4][+Fall,-J3]	Kiparskian mystery constraint	Kiparskyan mystery constraint, nonlexical	Kiparsky 1977	 1.701 	11	 18 
48	*[+Strong,+Accent,-J4][+Fall,-J2]		Kiparskyan mystery constraint, lexical	Kiparsky 1977
49	*[-Rise][+Strong][+wb]	Final foot strict	Must rise in last foot	Tarlinskaja?	 1.683 	215	 9 
50	*[+Fall][+Strong][+wb]		Don't fall in last foot	Bridges p. 41, Sprott p. 102
51	*[+Strong,-Accent][+wb]		Position 10 must be stressed (ignores extrametricals)	Tyrwhitt, cited in Bridges p. 84
52	*[+Strong,-Accent][][+Strong,-Accent]	Conjoined stress gap	Conjoined stress gap	Sprott p. 108; HK 1966:202	 1.34 	158	 11 
53	*[+J5,+Realsyl][]	Line cohesion	Internal I break	tradition	 0.854 	826	 6 
54	*[+J4,+Realsyl][]		Internal P break	tradition	 0.186 	3,175	 21 
55	*[+J3,+Realsyl][]		Internal CG break	symmetrical extension tradition
56	*[+J2,+Realsyl][]		Internal W break	symmetrical extension tradition
57	*[-Strong,+J5][]	Foot cohesion	Foot-internal I break	Implicit in Kiparsky 1977	 0.48 	195	 22 
58	*[-Strong,+J4][]		Foot-internal P break	Implicit in Kiparsky 1977	 0.41 	1,146	 8 
59	*[-Strong,+J3][]		Foot-internal CG break	Implicit in Kiparsky 1977
60	*[-Strong,+J2][]		Foot-internal Word break	Implicit in Kiparsky 1977
61	*[+J5][][+wb]		I break internal to last foot	See Sprott 126	 2.58 	4	 16 
62	*[+J4][][+wb]		P break internal to last foot	tradition
63	*[+J3][][+wb]		CG break internal to last foot	tradition
64	*[+J2][][+wb]		W break internal to last foot	Youmans 1989?
65	*[-Realsyl][+J5]		I break internal to first foot	See Bridges, p. 44, Sprott 126
66	*[-Realsyl][+J4]		P break internal to first foot	tradition	 0.807 	50	 20 
67	*[-Realsyl][+J3]		CG break internal to first foot	tradition	 0.672 	211	 13 
68	*[-Realsyl][+J2]		W break internal to first foot	parameters
69	*[-J2][+wb]	*Run-on line	Run-on line:  W (xxx is this in the inviolables?)	tradition	 11.097 	0	 25 
70	*[-J3][+wb]		Run-on line:  CG	tradition	 3.092 	7	 10 
71	*[-J4][+wb]		Run-on line:  P-phrase	tradition
72	*[-J5][+wb]		Run-on line:  I-phrase	tradition	 2.523 	302	 3 
73	*[+Strong,-J5]	*Run-on foot	Run-on foot:  W	parameters
74	*[+Strong,-J4]		Run-on foot:  CG	parameters
75	*[+Strong,-J3]		Run-on foot:  P-phrase	parameters
76	*[+Strong,-J2]		Run-on foot:  I-phrase	parameters
77	*[-Fall][-Strong][+wb]	*Extrametrical	*Non-falling extrametrical sequence	Kiparsky 1977?	 14.218 	0	 14 
78	*[-Strong,+Accent][+wb]		*stressed syllable in extrametrical	Kiparsky 1977?	 9.865 	0	 26 
79	*[+J2][-Strong][+wb]		Extrametricals must be in same word	??	 2.424 	14	 15 
80	*[-Strong][+wb]		*Extrametrical	tradition	 2.606 	162	 4 
81	*[-Strong,-J5][+wb]		conjoined constraint governing extrametricals - J5 version	Kiparsky 1977
82	*[-Strong,-J4][+wb]		conjoined constraint governing extrametricals - J4 version	Kiparsky 1977
83	*[+J3][-Accent,+J3]	Phonology	Stressless CG	phonology	 3.389 	99	 2 
84	*[+J4][-Accent,+J4]		Stressless P-phrase	phonology
85	*[+J5][-Accent,+J5]		Stressless I-phrase	phonology
86	*[-Accent,-J2][-Accent,-J2][-Accent]		Triple lapse within word	phonology	 13.57 	0	 17 
87	*[+J2][-Accent,-J2][-Accent,-J2]		Words cannot begin with two stressless syllables	phonology	 20.81 	0	 7 
88	zzzControlFile	 -features	FeaturesMetrics.txt
89	zzzControlFile	 -corpus	TrainingDataShakespeareBruce.txt
90	zzzControlFile	 -sigma2	1.00E+10
91	zzzControlFile	 -maxGramSize	3
92	zzzControlFile	 -minWordLength	11
93	zzzControlFile	 -maxWordLength	12
94	zzzControlFile	 -inviolable	InviolableConstraintsMetrics.txt
95	zzzControlFile	 #-grammar	Stage2Grammar.txt
96	zzzControlFile	 -train	
97	zzzControlFile	 -alpha	0.15
98	zzzControlFile	 -prune	
99	zzzControlFile	 #-complementClasses	
100	zzzControlFile	 #-select	
101	zzzControlFile	 -selectFromList	GrammarUG87Constraints.txt
102	zzzControlFile	 -test	TestingDataShakespeareBruce.txt
103	zzzControlFile	 -prePrune	
104	zzzControlFile	 -doNotAddStarSeg	
105	zzzzPlog of entire grammar	
