#Getting segmental bigrams from Celex
library(ngram)
library(tidyverse)

#Setting the working directory and reading in data
setwd("/Users/Karthik/Documents/Research/CorporaAnalyses And Datasets/Celex")

OpenCelexModified = function(corpus="CelexWordData.csv",address="/Users/Karthik/Documents/Research/CorporaAnalyses And Datasets/Celex/"){
  read.csv(paste0(address,corpus)) %>% 
    select(-Word.ID,-Lemma.ID) %>% 
    # distinct() %>%                             #There are lots of duplicates
    mutate(IPANew = gsub("'","",IPA),
           IPANew = gsub("\"","",IPANew),
           IPANew = gsub("-","",IPANew),
           IPANew = paste0("%",IPANew),
           IPANew = paste0(IPANew,"%"))
}


#segment bigram word-probability
segmentBigramWordProb = function(corpus=celexOriginal,word="%st%",log=T){
  #Getting segment ngrams in the corpus
  # corpus=OpenCelexModified()
  # word="%stIn#"
  # ngrams_raw=ngram(c("abcd","cdefgh"),n=2,sep="")
  ngrams_raw_corpus=ngram(corpus$IPANew,n=2,sep="")
  (ngramsCounts_corpus=get.phrasetable(ngrams_raw_corpus) %>% 
      separate(ngrams,into=paste0("Segment",1:2),sep=" ",remove=FALSE) %>% 
      mutate(TotalCount = sum(freq)))
  
  #Getting segment ngrams in the word
  ngrams_raw_word=ngram(word,n=2,sep="")
  (ngramsCounts_word=get.phrasetable(ngrams_raw_word) %>% 
      separate(ngrams,into=paste0("Segment",1:2),sep=" ",remove=FALSE) %>% 
      select(-prop))
  
  #Getting conditional probability of each segmental ngram in the word from the corpus
  bigramProbs=ngramsCounts_corpus %>% 
    filter(Segment1 %in% ngramsCounts_word$Segment1) %>% 
    left_join(ngramsCounts_word,by=paste0("Segment",1:2),suffix = c(".corpus", ".word")) %>% 
    group_by(Segment1) %>% 
    nest() %>% 
    mutate(Probability = map(data,function(x=data){(x$freq.corpus[!is.na(x$ngrams.word)]/sum(x$freq.corpus))^x$freq.word[!is.na(x$ngrams.word)]})) %>% 
    select(Segment1,Probability) %>% 
    unnest(Probability)
    
  #Final segment bigram probability of the word 
  if(log){
    list(ngrams=bigramProbs,wordProbability=log(prod(bigramProbs$Probability)))
  }else{
    list(ngrams=bigramProbs,wordProbability=prod(bigramProbs$Probability))
  }
}
#segmentBigramWordProb()
#segmentBigramWordProb(word="%stIn%")
#segmentBigramWordProb(word="%bl1f%")


#segment ngram word-probability
segmentNgramWordProb = function(corpus=celexOriginal,word="%st%",ngramSize=3,log=T){
  #Getting segment ngrams in the corpus
  # corpus=celexOriginal
  # ngramSize=4
  # word="%stIn#"
  # ngrams_raw=ngram(c("abcd","cdefgh"),n=ngramSize,sep="")
  # get.phrasetable(ngrams_raw)
  ngrams_raw_corpus=ngram(corpus$IPANew,n=ngramSize,sep="")
  (ngramsCounts_corpus=get.phrasetable(ngrams_raw_corpus) %>% 
      separate(ngrams,into=paste0("Segment",1:ngramSize),sep=" ",remove=FALSE) %>% 
      mutate(TotalCount = sum(freq)) %>% 
      # filter(row_number()<3) %>%
      unite("Context",Segment1:!!paste0("Segment",ngramSize-1),sep=" "))
  
  #Getting segment ngrams in the word
  ngrams_raw_word=ngram(word,n=ngramSize,sep="")
  (ngramsCounts_word=get.phrasetable(ngrams_raw_word) %>% 
      separate(ngrams,into=paste0("Segment",1:ngramSize),sep=" ",remove=FALSE) %>% 
      unite("Context",Segment1:!!paste0("Segment",ngramSize-1),sep=" "))
 
  #Getting conditional probability of each segmental ngram in the word from the corpus
  NgramProbs=ngramsCounts_corpus %>% 
    filter(Context %in% ngramsCounts_word$Context) %>% 
    left_join(ngramsCounts_word,by=c("Context",paste0("Segment",ngramSize)),suffix = c(".corpus", ".word")) %>% 
    group_by(Context) %>% 
    nest() %>% 
    mutate(Probability = map(data,function(x=data){(x$freq.corpus[!is.na(x$ngrams.word)]/sum(x$freq.corpus))^x$freq.word[!is.na(x$ngrams.word)]})) %>% 
    select(Context,Probability) %>% 
    unnest(Probability)
  
  #Final segment bigram probability of the word 
  if(log){
    list(ngrams=NgramProbs,wordProbability=log(prod(NgramProbs$Probability)))
  }else{
    list(ngrams=NgramProbs,wordProbability=prod(NgramProbs$Probability))
  }
}
#segmentNgramWordProb()
#segmentNgramWordProb(word="%stIn%",ngramSize=2)
#segmentNgramWordProb(word="%stIn%",ngramSize=4)
#segmentNgramWordProb(word="%bl1f%",ngramSize=2)
