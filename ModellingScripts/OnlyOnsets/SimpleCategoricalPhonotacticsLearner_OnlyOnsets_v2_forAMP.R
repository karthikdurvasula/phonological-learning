#This version incorporates all the functions from the AnalysisFunctions.R file.
#So, it is a cleaned up version.
#This version includes uni-grams and bi-grams, and works for onsets
#This script tries to learn simple phonotactic sequences
#Also see Freyer's (2017; MA Thesis) phonotactic learner model.
#But without weights or probabilities
#http://bir.brandeis.edu/bitstream/handle/10192/33888/FreyerThesis2017.pdf?sequence=5

#Logic of feature ngram finder:
# 1) Find all the segment ngrams of the relevant size (1/2/3/4/...)
# 2) Then find all the feature ngrams in each of the segment ngrams.
# 3) Throughout, ngramSize = both segmentNgram size and featureNgram size.

#NOTE: This models onsets ONLY!

library(tidyverse)
library(broom)
library(stringr)
library(xlsx)
source("AnalysisFunctions.R")

#************
#Data for testing

#Feature info
#This feature chart is only for consonants. So, the vowels get ignored completely
# HayesFeatures = read.csv("PhonologicalGeneralisations/HayesFeaturesConsonants_Contrastive.txt",sep="\t") %>%
#   rename(arpabet=X) %>%
#   #To keep it identical to the other feature chart
#   mutate(segment=tolower(arpabet)) %>%
#   gather("Feature","Value",-c(segment,arpabet)) %>%
#Even though it has -ve valued features, it is equivalent to an underspecified one, if -ve is replaced with 0.
HayesFeatures = read.xlsx("Features/HayesFeaturesConsonants.xlsx",sheetIndex=1) %>%   #Used this for AMP
  gather("Feature","Value",-c(arpabet,segment)) %>%
  mutate(FullFeature=paste(Value,Feature,sep="")) %>%
  as_tibble() %>%
  add_row(segment="#",arpabet="#",Feature="",Value="",FullFeature="#") %>%
  select(arpabet,FullFeature)

#CMU Dictionary pronunciation info
#Reading data from the website
CMUDictionary=tibble(rows=readLines("http://svn.code.sf.net/p/cmusphinx/code/trunk/cmudict/cmudict.0.7a")) %>%
  slice(119:n()) %>%
  separate(rows,c("Word","Transcription"),"  ") %>%
  mutate(Transcription = paste0("# ", Transcription," #"))
#View(CMUDictionary)

#CMU Dictionary onsets only pronunciation info - this is the selected subset used by Hayes and Wilson
CMUDictionaryOnsets = read.csv("PhonotacticLearnerUCLA/EnglishOnsets/EnglishLearningData.txt",sep="\t",header=F) %>%
  rename(Transcription = V1) %>%
  mutate(Transcription = paste("#",Transcription)) %>%
  uncount(V2) %>%
  # unnest() %>%
  mutate(Word = paste0("Word",row_number()))

#Getting all logically possible unigrams and bigrams
AllLogicallyPossibleNgrams=AllPossibleSegmentNgrams(ngramSizes=c(1:2))

#**************
#Albright (2007) Results - http://www.mit.edu/~albright/papers/Albright-BiasedGeneralization.pdf
# (Albright2007 = tibble(Word = c("thwack","plake","pleen","pleek","plim","blute","blodd","bluss","blad","blemp","blig","prundge","prupt","presp","brelth","brenth","pwet","pwist","pwuss","pwadd","pwuds","bwudd","bwadd","bwodd","pnep","pneek","pneen","bnuss","bneen","bnodd","bzuss","bzeen","bzike","bzodd","pteen","ptad","ptuss","ptep","bdute","bduss","bdeek"),
#                       #Transcription = c("# P L EY K", "# P L IY N #", "# P L IY K #", "# P L IH M #", "# B L UW T #", "# B L AA D #", "# B L AH S #", "# B L AE D #", "# B L EH M P #", "# B L IH G #", "# P R AH N JH #", "# P R AH P T #", "# P R EH S P #", "# B R EH L TH #", "# B R EH N TH #", "# P W EH T #", "# P W IH S T #", "# P W AH S #", "# P W AE D #", "# P W AH D Z #", "# B W AH D #", "# B W AE D #", "# B W AA D #", "# P N EH P #", "# P N IY K #", "# P N IY N #", "# B N AH S #", "# B N IY N #", "# B N AA D #", "# B Z AH S #", "# B Z IY N #", "# B Z AY K #", "# B Z AA D #", "# P T IY N #", "# P T AE D #", "# P T AH S #", "# P T EH P #", "# B D UW T #", "# B D AH S #", "# B D IY K #"),
#                       Transcription = c("# TH W", "# P L", "# P L", "# P L", "# P L", "# B L", "# B L", "# B L", "# B L", "# B L", "# B L", "# P R", "# P R", "# P R", "# B R", "# B R", "# P W", "# P W", "# P W", "# P W", "# P W", "# B W", "# B W", "# B W", "# P N", "# P N", "# P N", "# B N", "# B N", "# B N", "# B Z", "# B Z", "# B Z", "# B Z", "# P T", "# P T", "# P T", "# P T", "# B D", "# B D", "# B D"),
#                       MeanRating_Albright2007Model = c(NA, 4.94,5.32,5.06,4.71,4.84,5.13,4.67,4.65,4.69,4.58,4.94,4.07,4.50,3.14,4.11,2.53,2.94,2.61,2.89,2.17,2.94,2.41,2.94,2.00,1.76,2.16,2.06,2.39,2.00,1.81,2.00,1.28,1.63,2.44,1.67,1.94,1.86,1.71,1.71,1.72)) %>% 
#   filter(Word != "thwack") %>%  # This was just a check on my part
#   mutate(SimpleTranscription = str_replace_all(Transcription,"# ","")))
# write_csv(data.frame(Albright2007$SimpleTranscription),path="PhonotacticLearnerUCLA/EnglishOnsets/TestingData_Albright2007.txt",col_names=FALSE)
NgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetNGramsInTrainingCorpus.csv")
SegmentNgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetSegmentNGramsInTrainingCorpus.csv")


#Preprocessing the data
# Dataset=DataPreprocessor(dataName="Albright2007",NgramCountsFromTrainingCorpus=NgramCountsTrainingCorpus)
#Plots
AMPPlotterComparisonOfLearners(data=DataPreprocessor(dataName="Albright2007",
                                                     NgramCountsFromTrainingCorpus=NgramCountsTrainingCorpus,
                                                     addMissingSegmentNGramCount=T),
                               learner="SimplePhonotacticLearner")
                               # learner="All")
                               # learner="Hayes&Wilson2008")

#Trying plots and correlations for other measures in the dataset
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbSimpleSum),ExponentiatedProbSimpleSum,colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,nGramsNotPresent),nGramsNotPresent,colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ModifiednGramsNotPresent),log(ModifiednGramsNotPresent),colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbModifiednGramsNotPresent),ExponentiatedProbModifiednGramsNotPresent))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbLogModifiednGramsNotPresent),ExponentiatedProbLogModifiednGramsNotPresent))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
#Plots by prediction
ggplot(Dataset,aes(ExponentiatedProbSimpleSum,OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
ggplot(Dataset,aes(nGramsNotPresent,OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
ggplot(Dataset,aes(log(nGramsNotPresent),OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
#Correlations by prediction
# Dataset = Dataset %>%
  # filter(grossStatus=="invalid")
  # filter(Word != "thwack")
(with(Dataset,cor.test(ExponentiatedProbSimpleSum,OriginalRating,method="pearson")))
(with(Dataset,cor.test(nGramsNotPresent,OriginalRating,method="pearson")))

summary(lm(data=Dataset,OriginalRating~MaxEntScore))
summary(lm(data=Dataset,OriginalRating~log(ModifiednGramsNotPresent)))
summary(lm(data=Dataset,OriginalRating~exp(-log(ModifiednGramsNotPresent))))


#**************
#Scholes (1966) Results
#Reading in data
NgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetNGramsInTrainingCorpus.csv")
SegmentNgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetSegmentNGramsInTrainingCorpus.csv")

#Preprocessing the data
# Dataset=DataPreprocessor(dataName="Scholes1966",NgramCountsFromTrainingCorpus=NgramCountsTrainingCorpus)
#Plots
AMPPlotterComparisonOfLearners(data=DataPreprocessor(dataName="Scholes1966",
                                                         NgramCountsFromTrainingCorpus=NgramCountsTrainingCorpus,
                                                         addMissingSegmentNGramCount=T),
                               learner="SimplePhonotacticLearner")
                              # learner="All")
                              learner="Hayes&Wilson2008")

#Trying plots and correlations for other measures in the dataset
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbSimpleSum),ExponentiatedProbSimpleSum,colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,nGramsNotPresent),nGramsNotPresent,colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ModifiednGramsNotPresent),log(ModifiednGramsNotPresent),colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbModifiednGramsNotPresent),ExponentiatedProbModifiednGramsNotPresent))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbLogModifiednGramsNotPresent),ExponentiatedProbLogModifiednGramsNotPresent))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
#Plots by prediction
ggplot(Dataset,aes(ExponentiatedProbSimpleSum,OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
ggplot(Dataset,aes(nGramsNotPresent,OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
ggplot(Dataset,aes(log(nGramsNotPresent),OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))

#Correlations by prediction
TemperatureParameter(predictions=Dataset$MaxEntScore,
                     actual=Dataset$OriginalRating)
# Dataset = Dataset %>%
# filter(grossStatus=="invalid")
# filter(Word != "thwack")
(with(Dataset,cor.test(ExponentiatedProbSimpleSum,OriginalRating,method="pearson")))
(with(Dataset,cor.test(nGramsNotPresent,OriginalRating,method="pearson")))

summary(lm(data=Dataset,OriginalRating~MaxEntScore))
summary(lm(data=Dataset,OriginalRating~log(ModifiednGramsNotPresent)))
summary(lm(data=Dataset,OriginalRating~exp(-log(ModifiednGramsNotPresent))))

#**************
#On new clusters and SSP in English - Daland et al (2011)
NgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetNGramsInTrainingCorpus.csv")
SegmentNgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetSegmentNGramsInTrainingCorpus.csv")

#Preprocessing the data
# Dataset=PlottingPreprocessor(dataName="Dalandetal2011",NgramCountsFromTrainingCorpus=NgramCountsTrainingCorpus)
#Plots
AMPPlotterComparisonOfLearners(data=DataPreprocessor(dataName="Dalandetal2011",NgramCountsFromTrainingCorpus=NgramCountsTrainingCorpus,
                               addMissingSegmentNGramCount=T),
                               # learner="SimplePhonotacticLearner")
                               learner="All")
                               learner="Hayes&Wilson2008")

#Trying plots and correlations for other measures in the dataset
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbSimpleSum),ExponentiatedProbSimpleSum,colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,nGramsNotPresent),nGramsNotPresent,colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ModifiednGramsNotPresent),log(ModifiednGramsNotPresent),colour=grossStatus))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbModifiednGramsNotPresent),ExponentiatedProbModifiednGramsNotPresent))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
ggplot(Dataset,aes(reorder(Transcription,ExponentiatedProbLogModifiednGramsNotPresent),ExponentiatedProbLogModifiednGramsNotPresent))+
  geom_boxplot()+geom_text(aes(label=OnsetIPA))
#Plots by prediction
ggplot(Dataset,aes(ExponentiatedProbSimpleSum,OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
ggplot(Dataset,aes(nGramsNotPresent,OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))
ggplot(Dataset,aes(log(nGramsNotPresent),OriginalRating))+
  geom_smooth(method="lm")+geom_text(aes(label=OnsetIPA,colour=grossStatus))

#Correlations by prediction
TemperatureParameter(predictions=Dataset$MaxEntScore,
                     actual=Dataset$OriginalRating)
# Dataset = Dataset %>%
# filter(grossStatus=="invalid")
# filter(Word != "thwack")
(with(Dataset,cor.test(ExponentiatedProbSimpleSum,OriginalRating,method="pearson")))
(with(Dataset,cor.test(nGramsNotPresent,OriginalRating,method="pearson")))

summary(lm(data=Dataset,OriginalRating~MaxEntScore))
summary(lm(data=Dataset,OriginalRating~log(ModifiednGramsNotPresent)))
summary(lm(data=Dataset,OriginalRating~exp(-log(ModifiednGramsNotPresent))))

#**************
#On new clusters and SSP in English.
#It gets the predicted gradience for SSP violation ins English.
(SSPEnglish = tibble(Word = c("lpass","ptass","pnass"),
                     Transcription = c("# L P", "# P T", "# P N")))

NgramCountsTrainingCorpus = read_csv("PhonologicalGeneralisations/SimpleCategoricalPhonotacticsLearner_OnsetNGramsInTrainingCorpus.csv")

(Comparison_SSPEnglish=SSPEnglish %>% 
    group_by(Word) %>%
    nest() %>%
    mutate(ngrams_1=map(data,SegmentNgramsInWord,ngramSize=1),
           # ngrams_3=map(data,SegmentNgramsInWord,ngramSize=3),
           ngrams_2=map(data,SegmentNgramsInWord,ngramSize=2)) %>%
    gather(key="ngramType",value="segmentNgrams",ngrams_1:ngrams_2) %>% 
    unnest() %>% 
    left_join(AllLogicallyPossibleNgrams,by="segmentNgrams") %>% 
    group_by(Word,Transcription,ngramType,FeatureNgrams) %>% 
    count() %>%
    group_by(Word,Transcription) %>%
    nest() %>%
    mutate(CorpusNgrams=map(data,function(x){x %>% left_join(NgramCountsTrainingCorpus,by="FeatureNgrams")})) %>% 
    unnest() %>% 
    #Check at this point
    # X=Comparison_Albright2007 %>% 
    #   filter(is.na(CorpusN)) %>% 
    #   select(Word,Transcription,ngramType,FeatureNgrams) %>% 
    #   group_by(Word) %>% 
    #   mutate(nGramsNotPresent = length(FeatureNgrams)) %>% 
    #   arrange(nGramsNotPresent)
    # filter(Word == "bsoop") %>% 
    group_by(Word,Transcription) %>% 
    summarise(nGramsNotPresent = sum(is.na(CorpusN)),
              #ModifiednGramsNotPresent is to avoid 0 in the count, which will mess up the log calculation
              #The second +1 is for back segment bigrams.
              ModifiednGramsNotPresent = nGramsNotPresent+1 + 1 ,
              LogProbabilityOfSegmentSequenceWithNA = sum(log(CorpusProbability)),
              LogProbabilityOfSegmentSequence = sum(log(CorpusProbability),na.rm=T)) %>%
    #Note: LogProbabilityOfSegmentSequence is useless right now, as it uses both unigram and bigram probabilities.
    mutate(word = str_replace_all(Transcription,"# ",""),
           word = str_replace_all(word," ","")) %>% 
    ungroup() %>%
    mutate(ExponentiatedProbModifiednGramsNotPresent=exp(-ModifiednGramsNotPresent),
           ExponentiatedProbLogModifiednGramsNotPresent=exp(-log(ModifiednGramsNotPresent))) %>% 
    arrange(ModifiednGramsNotPresent))
# mutate(ExponentiatedProbModifiednGramsNotPresent=exp(-ModifiednGramsNotPresent)/sum(exp(-ModifiednGramsNotPresent)),
#        ExponentiatedProbLogModifiednGramsNotPresent=exp(-log(ModifiednGramsNotPresent))/sum(exp(-log(ModifiednGramsNotPresent)))))

#**************
#SSP Korean - toy set - extend to full later
#Prediction doesn't work!
(KoreanOnsets = data.frame(Transcription = c("# B W", "# B Y", "# P W", "# P Y", "# T W", "# T Y", "# S W", "# S Y"),
                           Word = c("bw","by","pw","py","tw","ty","sw","sy")))

# 505+255025
NgramCountsTrainingCorpusKorean = FeatureNgramsInCorpus(corpus=KoreanOnsets)

#On new clusters and SSP in Korean
(SSPKorean = tibble(Word = c("lpass","ptass","pnass"),
                    Transcription = c("# N B", "# B D", "# B N")))

(Comparison_SSPKorean=SSPKorean %>% 
    group_by(Word) %>%
    nest() %>%
    mutate(ngrams_1=map(data,SegmentNgramsInWord,ngramSize=1),
           # ngrams_3=map(data,SegmentNgramsInWord,ngramSize=3),
           ngrams_2=map(data,SegmentNgramsInWord,ngramSize=2)) %>%
    gather(key="ngramType",value="segmentNgrams",ngrams_1:ngrams_2) %>% 
    unnest() %>% 
    left_join(AllLogicallyPossibleNgrams,by="segmentNgrams") %>% 
    group_by(Word,Transcription,ngramType,FeatureNgrams) %>% 
    count() %>%
    group_by(Word,Transcription) %>%
    nest() %>%
    mutate(CorpusNgrams=map(data,function(x){x %>% left_join(NgramCountsTrainingCorpusKorean,by="FeatureNgrams")})) %>% 
    unnest() %>% 
    #Check at this point
    # X=Comparison_Albright2007 %>% 
    #   filter(is.na(CorpusN)) %>% 
    #   select(Word,Transcription,ngramType,FeatureNgrams) %>% 
    #   group_by(Word) %>% 
    #   mutate(nGramsNotPresent = length(FeatureNgrams)) %>% 
    #   arrange(nGramsNotPresent)
    # filter(Word == "bsoop") %>% 
    group_by(Word,Transcription) %>% 
    summarise(nGramsNotPresent = sum(is.na(CorpusN)),
              #ModifiednGramsNotPresent is to avoid 0 in the count, which will mess up the log calculation
              ModifiednGramsNotPresent = nGramsNotPresent+1,
              LogProbabilityOfSegmentSequenceWithNA = sum(log(CorpusProbability)),
              LogProbabilityOfSegmentSequence = sum(log(CorpusProbability),na.rm=T)) %>%
    #Note: LogProbabilityOfSegmentSequence is useless right now, as it uses both unigram and bigram probabilities.
    mutate(word = str_replace_all(Transcription,"# ",""),
           word = str_replace_all(word," ","")) %>% 
    ungroup() %>%
    mutate(ExponentiatedProbModifiednGramsNotPresent=exp(-ModifiednGramsNotPresent),
           ExponentiatedProbLogModifiednGramsNotPresent=exp(-log(ModifiednGramsNotPresent))))
    # mutate(ExponentiatedProbModifiednGramsNotPresent=exp(-ModifiednGramsNotPresent)/sum(exp(-ModifiednGramsNotPresent)),
    #        ExponentiatedProbLogModifiednGramsNotPresent=exp(-log(ModifiednGramsNotPresent))/sum(exp(-log(ModifiednGramsNotPresent)))))