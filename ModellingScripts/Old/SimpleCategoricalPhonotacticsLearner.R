#This script tries to learn simple phonotactic sequences
#Similar to, Freyer's (2017; MA Thesis) phonotactic learner model.
#But without weights or probabilities
#http://bir.brandeis.edu/bitstream/handle/10192/33888/FreyerThesis2017.pdf?sequence=5

library(tidyverse)
library(stringr)
library(xlsx)

#Feature info
setwd("/Users/Karthik/Documents/Research/R-Scripts/ReseachScripts/PhonologicalGeneralisations")
HayesFeatures = read.xlsx("HayesFeaturesConsonants.xlsx",sheetIndex=1) %>%
  gather("Feature","Value",-c(segment,arpabet)) %>% 
  mutate(FullFeature=paste(Value,Feature,sep="")) %>%
  as_tibble() %>% 
  add_row(segment="#",arpabet="#",Feature="",Value="",FullFeature="#") %>% 
  select(arpabet,FullFeature)

#CMU Dictionary pronunciation info
#Reading data from the website
CMUDictionary=tibble(rows=readLines("http://svn.code.sf.net/p/cmusphinx/code/trunk/cmudict/cmudict.0.7a")) %>% 
  slice(119:n()) %>%
  separate(rows,c("Word","Transcription"),"  ") %>% 
  mutate(Transcription = paste0("# ", Transcription," #"))
#View(CMUDictionary)

#**************
#Functions
#Getting featural ngram for a single sequence
SingleFeatureNgram = function(sequence = "# HH",Features=HayesFeatures){
  
  #Getting NGram size
  ngramSize = str_count(sequence," ")+1

  #Getting segments
  segments=c(str_split_fixed(sequence," ",ngramSize))
  
  #Getting features of first 
  finalFeaturesList = as.vector(unlist(Features %>% filter(arpabet == segments[1]) %>% select(FullFeature) %>% rename(observedNgram=FullFeature)))
  #Getting rest of the features and then the combinations  
  if(ngramSize>=2){
    for(i in c(2:ngramSize)){
      #i=2
      finalFeaturesList_temp = as.vector(unlist(Features %>% filter(arpabet == segments[2]) %>% select(FullFeature) %>% rename(observedNgram=FullFeature)))
      finalFeaturesList = c(outer(finalFeaturesList,finalFeaturesList_temp,"paste"))
    }
  }
  
  #returning observed featural ngrams
  finalFeaturesList
}
SingleFeatureNgram(sequence = "# HH K")

#Gets the featural n-grams present in a word
SetOfFeatureNgrams = function(transcription="# P R AA D #",ngramSize=2,Features=HayesFeatures,Dictionary=CMUDictionary){

  #Getting all the start points for the ngram windows
  Points1 = c(1,gregexpr(" ",transcription)[[1]]+1)
  Points2 = c(gregexpr(" ",transcription)[[1]]-1,nchar(transcription))
  if(ngramSize==1){
    Ngrams = tibble(start = Points1,
                    end = Points2)
  }else{
    Ngrams = tibble(start = Points1[-c((length(Points1)-(ngramSize-2)):length(Points1))],
                    end = Points2[-c(1:ngramSize-1)])  
  }
  Ngrams = Ngrams %>% 
            mutate(Transcription = transcription,
                   ngram = substr(Transcription,start,end))
  
  allFeaturalNgrams=mapply(SingleFeatureNgram,Ngrams$ngram) %>% unlist() %>% as.vector()
  
  #return values
  allFeaturalNgrams
}
SetOfFeatureNgrams(ngramSize=1)
SetOfFeatureNgrams(ngramSize=2)

#Getting featural n-grams present in a corpus
FeatureNgramsInCorpus = function(corpus = c("prod","sad","stretch"),ngramWindows=c(1:4),Dictionary=CMUDictionary,removeStress=T){
  
  #Getting pronunciation
  pronunciation = Dictionary %>% filter(Word %in% toupper(corpus))
  
  #Removing Stress if chosen
  if(removeStress){
    pronunciation = pronunciation %>% mutate(Transcription=gsub("1","",Transcription),
                                             Transcription=gsub("2","",Transcription),
                                             Transcription=gsub("0","",Transcription))
  }
  
  ngrams=c()
  #getting n-grams
  for(i in ngramWindows){
    #print(i)
    ngrams_temp=tibble(window=i,ngrams=as.vector(unlist(mapply(function(x){SetOfFeatureNgrams(transcription=x,ngramSize=i)},pronunciation$Transcription))))
    ngrams=rbind(ngrams,ngrams_temp)
  }
  
  #returning ngrams
  ngrams %>% group_by(ngrams,window) %>% count() %>% arrange(desc(n))
}
#system.time(FeatureNgramsInCorpus(corpus = CMUDictionary$Word[sample(1:nrow(CMUDictionary),1e1)], ngramWindows=1:3))

NgramCounts=FeatureNgramsInCorpus(corpus = CMUDictionary$Word[sample(1:nrow(CMUDictionary),1e4)], ngramWindows=1:3)

#write_csv(NgramCounts,"SimpleCategoricalPhonotacticsLearner_NGrams.csv")
rm(list=ls())

#**************
setwd("/Users/Karthik/Documents/Research/R-Scripts/ReseachScripts/PhonologicalGeneralisations")
NgramCounts = read_csv("SimpleCategoricalPhonotacticsLearner_NGrams.csv")

#Testing new words
Testing = tibble(Word = c("vlad", "mroop", "froud", "bvit", "steim"),
                 Transcription = c("# V L AE D #", "# M R UW P #", "# F R OW D #", "# B V IH T", "# S T EY M #"))

Testing %>% 
  group_by(Word) %>% 
  nest() %>% 
  mutate(ngrams=map(data,SetOfFeatureNgrams,ngramSize=3)) %>% 
  select(-data) %>% 
  unnest() %>% 
  left_join(NgramCounts) %>% 
  group_by(Word) %>% 
  summarise(MeanCount = mean(n,na.rm=T),
            MinCount = min(n)) %>% 
  arrange(MinCount)


#**************
#Albright (2007) Results - http://www.mit.edu/~albright/papers/Albright-BiasedGeneralization.pdf
Albright2007 = tibble(Word = c("plake","pleen","pleek","plim","blute","blodd","bluss","blad","blemp","blig","prundge","prupt","presp","brelth","brenth","pwet","pwist","pwuss","pwadd","pwuds","bwudd","bwadd","bwodd","pnep","pneek","pneen","bnuss","bneen","bnodd","bzuss","bzeen","bzike","bzodd","pteen","ptad","ptuss","ptep","bdute","bduss","bdeek"),
                      Transcription = c("# P L EY K", "# P L IY N #", "# P L IY K #", "# P L IH M #", "# B L UW T #", "# B L AA D #", "# B L AH S #", "# B L AE D #", "# B L EH M P #", "# B L IH G #", "# P R AH N JH #", "# P R AH P T #", "# P R EH S P #", "# B R EH L TH #", "# B R EH N TH #", "# P W EH T #", "# P W IH S T #", "# P W AH S #", "# P W AE D #", "# P W AH D Z #", "# B W AH D #", "# B W AE D #", "# B W AA D #", "# P N EH P #", "# P N IY K #", "# P N IY N #", "# B N AH S #", "# B N IY N #", "# B N AA D #", "# B Z AH S #", "# B Z IY N #", "# B Z AY K #", "# B Z AA D #", "# P T IY N #", "# P T AE D #", "# P T AH S #", "# P T EH P #", "# B D UW T #", "# B D AH S #", "# B D IY K #"),
                      MeanRating = c(4.94,5.32,5.06,4.71,4.84,5.13,4.67,4.65,4.69,4.58,4.94,4.07,4.50,3.14,4.11,2.53,2.94,2.61,2.89,2.17,2.94,2.41,2.94,2.00,1.76,2.16,2.06,2.39,2.00,1.81,2.00,1.28,1.63,2.44,1.67,1.94,1.86,1.71,1.71,1.72))

Comparison_Albright2007=Albright2007 %>% 
  group_by(Word,MeanRating) %>% 
  nest() %>% 
  mutate(ngrams=map(data,SetOfFeatureNgrams,ngramSize=3)) %>% 
  select(-data) %>% 
  unnest() %>% 
  left_join(NgramCounts) %>% 
  group_by(Word,MeanRating) %>% 
  summarise(MeanCountSimpleModel = mean(n,na.rm=T),
            MinCountSimpleModel = min(n)) %>% 
  arrange(MinCountSimpleModel)

ggplot(Comparison_Albright2007,aes(MeanRating,MinCountSimpleModel))+
  geom_point()+geom_smooth(method="lm")
