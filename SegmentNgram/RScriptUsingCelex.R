#Getting segmental bigrams from Celex
library(ngram)
library(tidyverse)

#Setting the working directory and reading in data
setwd("/Users/Karthik/Documents/Research/CorporaAnalyses And Datasets/Celex")
celexOriginal = read.csv("CelexWordData.csv") %>% 
  select(-Word.ID,-Lemma.ID) %>% 
  distinct() %>%                             #There are lots of duplicates
  mutate(IPANew = gsub("'","",IPA),
         IPANew = gsub("\"","",IPANew),
         IPANew = gsub("-","",IPANew),
         IPANew = paste0("%",IPANew),
         IPANew = paste0(IPANew,"#"))


#segment bigram word-probability
segmentBigramWordProb = function(corpus=celexOriginal,word="%st#",log=T){
  #Getting segment ngrams in the corpus
  # corpus=celexOriginal
  # word="%stIn#"
  # ngrams_raw=ngram(c("abcd","cdefgh"),n=2,sep="")
  ngrams_raw_corpus=ngram(corpus$IPANew,n=2,sep="")
  (ngramsCounts_corpus=get.phrasetable(ngrams_raw_corpus) %>% 
      separate(ngrams,into=paste0("Segment",1:2),sep=" ",remove=FALSE) %>% 
      mutate(TotalCount = sum(freq)))
  
  #Getting segment ngrams in the word
  ngrams_raw_word=ngram(word,n=2,sep="")
  (ngramsCounts_word=get.phrasetable(ngrams_raw_word) %>% 
      separate(ngrams,into=paste0("Segment",1:2),sep=" ",remove=FALSE))
  
  #Getting conditional probability of each segmental ngram in the word from the corpus
  bigramProbs=ngramsCounts_corpus %>% 
    filter(Segment1 %in% ngramsCounts_word$Segment1) %>% 
    left_join(ngramsCounts_word,by=paste0("Segment",1:2),suffix = c(".corpus", ".word")) %>% 
    group_by(Segment1) %>% 
    nest() %>% 
    mutate(Probability = map(data,function(x=data){(x$freq.corpus[!is.na(x$ngrams.word)]/sum(x$freq.corpus))^x$freq.word[!is.na(x$ngrams.word)]})) %>% 
    select(Segment1,Probability) %>% 
    unnest(Probability)
    
  #Final segment bigram probability of the word 
  if(log){
    log(prod(bigramProbs$Probability))  
  }else{
    prod(bigramProbs$Probability)
  }
}
segmentBigramWordProb()
segmentBigramWordProb(word="%stIn#")
segmentBigramWordProb(word="%bl1f#")


#segment ngram word-probability
segmentNgramWordProb = function(corpus=celexOriginal,windowSize=2,word="%st#"){
  #Getting segment ngrams in the corpus
  # windowSize=2
  # corpus=celexOriginal
  # word="%st#"
  # ngrams_raw=ngram(c("abcd","cdefgh"),n=windowSize,sep="")
  ngrams_raw_corpus=ngram(corpus$IPANew,n=windowSize,sep="")
  (ngramsCounts_corpus=get.phrasetable(ngrams_raw_corpus))
  
  #Getting segment ngrams in the word
  ngrams_raw_word=ngram(word,n=windowSize,sep="")
  (ngramsCounts_word=get.phrasetable(ngrams_raw_word))
  
  #Getting conditional probability of each segmental ngram in the word from the corpus
  ngramsCounts_word = ngramsCounts_word %>% 
    separate(ngrams,into=paste0("Segment",1:windowSize),sep=" ",remove=FALSE) %>% 
    mutate(corpusProbability = grepl(""))
}