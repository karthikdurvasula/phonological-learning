A = AA 			(e.g. car)
i = IY				(e.g., bee)											
I = IH			(e.g., bit)											
oI = OY			(e.g., boy)
O = AO			(e.g., saw)
0 = N/A in US English, but coded as AA			(e.g., Bob)
3 = AH0		(e.g., about, first vowel)
u = UW			(e.g., boot)
aU = AW		(e.g., how)
aI = AY			(e.g., bye)
e = EH			(e.g., bed)
eI = EY			(e.g., bade)
^ = AH1			(e.g., bud)

T = TH			(e.g., three)
D = DH			(e.g., then)
t = t 				(e.g., ten)
S = SH			(e.g., she)
s = s 			(e.g., see)
tS = CH		(e.g., champ)
dZ = JH		(e.g., jam)
Z = ZH			(e.g., beige, last consonant)
N = NG			(e.g., bang, last consonant)
' = following syllable is stressed.

Stress rule: Always stress the tense vowels, and AA, AE and AH1? AH0 is always unstressed.


*******
That looks mostly right, except (1) we didn't have o = OW, as you have it there; the only use of o is I think in 

oI    as in boy

(2) a =/= AA; our use of a us in the digraph ae, which is the vowel in "cat" (kaet)

Our A is in fact your (ARPAbet) AA.

(3) We also have another vowel, 0 = the low back rounded vowel that Brit's have instead of US English AA (as in "Bob"). US English doesn't have our 0 vs A distinction, so you don't have a symbol for it in ARPAbet.

--- John
*******