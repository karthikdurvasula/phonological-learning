% !TEX encoding = UTF-8 Unicode
% !TEX TS-program = arara
% arara: xelatex: { shell: yes }
% arara: biber
% arara: xelatex: { synctex: yes, shell: yes }
% arara: clean: {files: [OGradienceWhencedoyoucome.aux, OGradienceWhencedoyoucome.idx, OGradienceWhencedoyoucome.out, OGradienceWhencedoyoucome.ilg, OGradienceWhencedoyoucome.ind, OGradienceWhencedoyoucome.log, OGradienceWhencedoyoucome.bbl, OGradienceWhencedoyoucome.bcf, OGradienceWhencedoyoucome.ist, OGradienceWhencedoyoucome.blg, OGradienceWhencedoyoucome.bcf, OGradienceWhencedoyoucome.run.xml, OGradienceWhencedoyoucome.synctex.gz]}

\documentclass[12pt]{article}

%----------------------------------------------------------------------------------------
% DOCUMENT STUFF
%----------------------------------------------------------------------------------------

\usepackage{amssymb} % for \checkmark
\usepackage[italic]{mathastext}
%\usepackage[T1]{fontenc}

%For in line IPA fonts
\usepackage{fontspec}
\setmainfont[Ligatures=TeX]{Times New Roman}
\newfontfamily\phonetic[]{Doulos SIL}

\usepackage{textglos}
\usepackage{tipa}

\usepackage[margin=1in,letterpaper]{geometry}

\usepackage{enumitem}

\usepackage{booktabs}

\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}}

\usepackage{graphicx}
\graphicspath{{../../figure/}}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}

\usepackage{tikz}
\usetikzlibrary{positioning}

\usepackage[textsize=tiny]{todonotes}

%To fix figures/tables in place
\usepackage{threeparttable}
\usepackage{float}

%----------------------------------------------------------------------------------------
% REFERENCES
%----------------------------------------------------------------------------------------

\usepackage[american]{babel}
\usepackage{csquotes}
\usepackage[
	backend=biber,
	citestyle=authoryear-comp,
	bibstyle=authoryear-comp, 
	maxcitenames=2,
	maxbibnames=99,
	dashed=false,
	url=false,
	labeldate=true,
	uniquename=minfull,
	uniquelist=minyear
]{biblatex}

% For possessive citations; this might(?) require Biblatex >= 3.3
% See:
% 	1. http://tex.stackexchange.com/q/22273/32888
%	2. http://tex.stackexchange.com/a/22337/32888
%	3. http://tex.stackexchange.com/a/307461/32888
\DeclareNameFormat{labelname:poss}{% Based on labelname from biblatex.def
  \nameparts{#1}% Not needed if using Biblatex 3.4
  \ifcase\value{uniquename}%
    \usebibmacro{name:family}{\namepartfamily}{\namepartgiven}{\namepartprefix}{\namepartsuffix}%
  \or
    \ifuseprefix
      {\usebibmacro{name:first-last}{\namepartfamily}{\namepartgiveni}{\namepartprefix}{\namepartsuffixi}}
      {\usebibmacro{name:first-last}{\namepartfamily}{\namepartgiveni}{\namepartprefixi}{\namepartsuffixi}}%
  \or
    \usebibmacro{name:first-last}{\namepartfamily}{\namepartgiven}{\namepartprefix}{\namepartsuffix}%
  \fi
  \usebibmacro{name:andothers}%
  \ifnumequal{\value{listcount}}{\value{liststop}}{'s}{}}
\DeclareFieldFormat{shorthand:poss}{%
  \ifnameundef{labelname}{#1's}{#1}}
\DeclareFieldFormat{citetitle:poss}{\mkbibemph{#1}'s}
\DeclareFieldFormat{label:poss}{#1's}
\newrobustcmd*{\posscitealias}{%
  \AtNextCite{%
    \DeclareNameAlias{labelname}{labelname:poss}%
    \DeclareFieldAlias{shorthand}{shorthand:poss}%
    \DeclareFieldAlias{citetitle}{citetitle:poss}%
    \DeclareFieldAlias{label}{label:poss}}}
\newrobustcmd*{\posscite}{%
  \posscitealias%
  \textcite}
\newrobustcmd*{\Posscite}{\bibsentence\posscite}
\newrobustcmd*{\posscites}{%
  \posscitealias%
  \textcites}

\DefineBibliographyStrings{american}{phdthesis = {PhD dissertation}} %For PhD Dissertations

\DeclareFieldFormat{doi}{%
  \ifhyperref
    {\textsc{doi}: \href{http://dx.doi.org/#1}{\nolinkurl{#1}}}
    {\textsc{doi}: \nolinkurl{#1}}}
    
\renewbibmacro{in:}{%
  \ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}}
  
\addbibresource{../../../../References/NewLibrary.bib}

%----------------------------------------------------------------------------------------
% HYPERREF
%----------------------------------------------------------------------------------------

\usepackage{hyperref}

%----------------------------------------------------------------------------------------
% META
%----------------------------------------------------------------------------------------

\title{O gradience, whence do you come?}
\author{Karthik Durvasula \\ Michigan State University}
\date{}

%----------------------------------------------------------------------------------------
% MACROS
%----------------------------------------------------------------------------------------

\newcommand*\samethanks[1][\value{footnote}]{\footnotemark[#1]}
\newcommand*{\IE}{\emph{i.e.},}
\newcommand*{\EG}{\emph{e.g.},}
\newcommand*{\ETC}{\emph{etc.}}
\newcommand*{\Function}[1]{\texttt{#1}}
\newcommand*{\Package}[1]{\texttt{#1}}
\newcommand*{\SP}{\emph{Subset Principle}}
\newcommand*{\SG}{\emph{Simplest Generalization}}
\newcommand*{\MSG}{\emph{Multiple Simple(st) Generalizations}}
\newcommand*{\PropSimple}{\emph{Proportional to Simplicity}}
\newcommand*{\PropSpec}{\emph{Proportional to Specificity}}


%*****************

\begin{document}
\sloppy
\maketitle

\noindent{}It has long been recognised that linguistic behaviour is a multi-factorial problem, with grammatical knowledge playing only a partial role in it \autocite{Chomsky1965, Valian1982, Schuetze1996}.
Furthermore, given that the interacting performance factors (perception, articulation, lexical access,\ldots{}) are very likely gradient, behavioral data such as acceptability judgments, production data, and perceptual responses are expected to be gradient with both categorical and gradient grammatical models.
Consequently, (gradient) behaviour cannot be directly interpreted, and methodologically one cannot automatically assume that the (gradient) behaviour reflects grammatical knowledge.

In this talk, I will present two cases studies that look into the issue of gradient behaviour. In the first case-study, I look at the issue of incomplete neutralisation which has been used to argue for non-categorical representations \autocite{portLeary2005}. Based on production data of a tone sandhi process in Huai’an Mandarin, I will argue that incomplete phonetic neutralization is observed even in a case where the neutralization must be phonologically complete; therefore, incomplete phonetic neutralisation is not diagnostic of gradient phonological representations, and the source of the incompleteness must be extra-phonological. 
In the second case-study, I look at the influence of type-frequency on acceptability judgements. I re-visit \posscite{hayes-wilson-2008} MaxEnt modelling of English onset clusters using \posscite{Scholes1966} classic acceptability judgement data, and show that type frequency of the onset sequence does not actually add to the model fit. 
This in turn raises the question of where exactly the gradience in acceptabilities comes from.
I will argue that both case-studies suggest that a more careful consideration of computational models and of interacting extra-grammatical factors is needed before gradience can be attributed to grammatical knowledge.


\printbibliography
\end{document}
