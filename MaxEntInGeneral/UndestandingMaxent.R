#This understands the Wug shapes that Hayes (2020) was talking about.

# Case study 1:
# two constraints: OnOff (0-1)  and Variable (1-7)
#Consider two candidates: One violates OnOff, and the other violates Variable
MaxEntSigmoid = function(Variable=1,OnOff=1){
  exp(-(OnOff*3.5+Variable))
}
x=seq(0,7,by=0.01)
(ProbOnOff_1 = MaxEntSigmoid(Variable=x,OnOff=0)/(MaxEntSigmoid(Variable=0,OnOff=1)+MaxEntSigmoid(Variable=x,OnOff=0)))
plot(x,ProbOnOff_1)


-log(0.5)
